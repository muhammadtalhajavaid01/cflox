import * as types from '../constants/actions-types';

const updateConversionHistory = history => (
  {
    type: types.UPDATE_CONVERSION_HISTORY,
    history,
  }
);

export default updateConversionHistory;
