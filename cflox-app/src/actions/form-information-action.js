import * as types from '../constants/actions-types';

const updateToggleStatus = status => (
  {
    type: types.UPDATE_TOGGLE_STATUS,
    status,
  }
);

export default updateToggleStatus;
