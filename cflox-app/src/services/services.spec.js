import { decimalToRoman } from './conversion';
import { isValidDecimal, isValidBinary, isValidHexaDecimal } from './validation';

describe('Decimal to Roman', () => {
  it('Decimal to Roman conversion check, 10.1', () => {
    expect(decimalToRoman(10.1)).toEqual('X');
  });
});

describe('Decimal to Roman', () => {
  it('Decimal to Roman conversion check, 20.1', () => {
    expect(decimalToRoman(20.1)).toEqual('XX');
  });
});

describe('Decimal to Roman', () => {
  it('Decimal to Roman conversion check, 1000.1', () => {
    expect(decimalToRoman(1000.1)).toEqual('M');
  });
});

describe('Binary to Roman', () => {
  const binaryToDecimal = parseInt(1001, 2);
  it('Binary to Roman conversion check, 1001', () => {
    expect(decimalToRoman(binaryToDecimal)).toEqual('IX');
  });
});

describe('Binary to Roman', () => {
  const binaryToDecimal = parseInt(1010, 2);
  it('Binary to Roman conversion check, 1010', () => {
    expect(decimalToRoman(binaryToDecimal)).toEqual('X');
  });
});

describe('Binary to Roman', () => {
  const binaryToDecimal = parseInt(1100, 2);
  it('Binary to Roman conversion check, 1100', () => {
    expect(decimalToRoman(binaryToDecimal)).toEqual('XII');
  });
});

describe('HexaDecimal to Roman', () => {
  const hexaToDecimal = parseInt(10, 16);
  it('Hexa to Roman conversion check, 10', () => {
    expect(decimalToRoman(hexaToDecimal)).toEqual('XVI');
  });
});

describe('HexaDecimal to Roman', () => {
  const hexaToDecimal = parseInt(12, 16);
  it('Hexa to Roman conversion check, 12', () => {
    expect(decimalToRoman(hexaToDecimal)).toEqual('XVIII');
  });
});

describe('HexaDecimal to Roman', () => {
  const hexaToDecimal = parseInt(16, 16);
  it('Hexa to Roman conversion check, 16', () => {
    expect(decimalToRoman(hexaToDecimal)).toEqual('XXII');
  });
});

describe('Is Valid Decimal', () => {
  it('Is valid decimal number - check - 100.1', () => {
    expect(isValidDecimal(100.1)).toEqual(true);
  });
});

describe('Is Valid Binary', () => {
  it('Is valid binary number - check - 1001', () => {
    expect(isValidBinary(1001)).toEqual(true);
  });
});

describe('Is Valid HexaDecimal', () => {
  it('Is valid hexadecimal number - check - 16', () => {
    expect(isValidHexaDecimal('16')).toEqual(true);
  });
});
