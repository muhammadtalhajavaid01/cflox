
export const isValidBinary = (num) => {
  const status = /^[01]+$/.test(num)
  return status;
};

export const isValidDecimal = (num) => {
  const status = /^\d+\.\d{0,2}$/.test(num);
  return status;
};

export const isValidHexaDecimal = (num) => {
  const number = parseInt(num, 16);
  return (number.toString(16) === num);
};
