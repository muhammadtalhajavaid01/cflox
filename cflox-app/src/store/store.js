import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import reducers from '../reducers/reducers';
import logger from './logger';

const store = createStore(reducers, applyMiddleware(thunk, logger));

export default store;
