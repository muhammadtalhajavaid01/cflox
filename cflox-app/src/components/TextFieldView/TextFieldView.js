import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const styles = {
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
};

const TextFieldView = ({
  classes,
  inputHandler,
  fieldTitle,
  inputText,
}) => {
  return (
    <TextField
      id="outlined-email-input"
      label={fieldTitle}
      value={inputText}
      className={classes.textField}
      type="email"
      name="email"
      margin="normal"
      variant="outlined"
      inputProps={{
        maxLength: 7,
      }}
      onChange={inputHandler.bind(this)}
    />
  );
};

TextFieldView.defaultProps = {
  classes: null,
};

TextFieldView.propTypes = {
  classes: PropTypes.shape({ object: PropTypes.object }),
  inputHandler: PropTypes.func.isRequired,
  fieldTitle: PropTypes.string.isRequired,
  inputText: PropTypes.string.isRequired,
};

export default withStyles(styles)(TextFieldView);
