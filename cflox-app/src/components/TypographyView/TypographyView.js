import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';

const TypographyView = (props) => {
  const { title } = props;

  return (
    <Typography variant="h5" component="h2">
      {title}
    </Typography>
  );
};

TypographyView.propTypes = {
  title: PropTypes.string.isRequired,
};

export default TypographyView;
