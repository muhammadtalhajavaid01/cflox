import React from 'react';
import PropTypes from 'prop-types';
import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css';

const SweetAlertView = (props) => {
  const {
    title,
    show,
    text,
    okHandler,
  } = props;

  return (
    <SweetAlert
      show={show}
      title={title}
      text={text}
      onConfirm={okHandler}
    />
  );
};


SweetAlertView.propTypes = {
  title: PropTypes.string.isRequired,
  show: PropTypes.bool.isRequired,
  okHandler: PropTypes.func.isRequired,
};

export default SweetAlertView;
