import React, { Component } from 'react';
import MultiToggle from 'react-multi-toggle';
require('./Style.css')

const groupOptions = [
  {
    displayName: 'Decimal',
    value: 3
  },
  {
    displayName: 'Binary',
    value: 2
  },
  {
    displayName: 'Hexadecimal',
    value: 1
  }
];

class ToggleView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      groupSize: 3
    };
  }

  onGroupSizeSelect = value => {
    this.setState({ groupSize: value });
    this.props.updateToggleStatus(value);
  }

  render = () => {
    const { groupSize } = this.state;
    return (
      <MultiToggle
        options={groupOptions}
        selectedOption={groupSize}
        onSelectOption={this.onGroupSizeSelect}
      />
    );
  }
}

export default ToggleView;
