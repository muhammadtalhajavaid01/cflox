import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import SendIcon from '@material-ui/icons/Send';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});

const IconLabelButtons = (props) => {
  const { classes, convertHandler } = props;
  return (
    <Button variant="contained" color="primary" className={classes.button} onClick={convertHandler}>
      Convert
      <SendIcon className={classes.rightIcon} />
    </Button>

  );
};

IconLabelButtons.defaultProps = {
  classes: null,
};

IconLabelButtons.propTypes = {
  classes: PropTypes.shape({ object: PropTypes.object }),
  convertHandler: PropTypes.func.isRequired,
};

export default withStyles(styles)(IconLabelButtons);
