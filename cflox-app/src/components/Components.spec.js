import React from 'react';
import { shallow } from 'enzyme';

import ListItemView from './ListItem/ListItemView';
import TypographyView from './TypographyView/TypographyView';
import TextFieldView from './TextFieldView/TextFieldView';

it('Creating List View Item', () => {
  const data = {
    format: 'Decimal',
    input: '10.1',
    convertedValue: 'X',
    dateTime: new Date(),
  };

  const wrapper = shallow(<ListItemView historyItem={data} />);
  expect(wrapper.find('.li').exists());
});

it('Creating Typrography View', () => {
  const wrapper = shallow(<TypographyView title="Hexadecimal to Roman" />);
  expect(wrapper.find('h2').exists());
});

it('Creating Text Field View', () => {
  const wrapper = shallow(<TextFieldView inputHandler={() => {}} fieldTitle="Text View" inputText="12.1" />);
  expect(wrapper.find('.MuiTypography').exists());
});
