import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';

const styles = theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
});

const pStyle = {
  overflow: 'auto',
};

const ListItemView = (props) => {
  const { historyItem } = props;
  return (
    <ListItem style={pStyle}>
      <Avatar>
        <ImageIcon />
      </Avatar>
      <ListItemText primary={`${historyItem.input} ( ${historyItem.format} ) > ${historyItem.convertedValue}`} secondary={` ${historyItem.dateTime}`} />
    </ListItem>
  );
};

ListItemView.defaultProps = {
  historyItem: null,
};

ListItemView.propTypes = {
  historyItem: PropTypes.shape({ object: PropTypes.object }),
};

export default withStyles(styles)(ListItemView);
