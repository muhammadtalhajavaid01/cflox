import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import TypographyView from '../../components/TypographyView/TypographyView';

const styles = {
  root: {
    flexGrow: 1,
  },
};

const AppBarContainer = (props) => {
  const { classes, title } = props;

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Toolbar>
          <TypographyView title={title} />
        </Toolbar>
      </AppBar>
    </div>
  );
};

AppBarContainer.defaultProps = {
  classes: null,
};

AppBarContainer.propTypes = {
  classes: PropTypes.shape({ object: PropTypes.object }),
  title: PropTypes.string.isRequired,
};

export default withStyles(styles)(AppBarContainer);
