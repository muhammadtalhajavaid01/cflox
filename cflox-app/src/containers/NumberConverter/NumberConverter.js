import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextFieldView from '../../components/TextFieldView/TextFieldView';
import TypographyView from '../../components/TypographyView/TypographyView';
import IconLabelButtons from '../../components/ButtonView/ButtonView';
import ToggleView from '../../components/ToggleView/ToggleView';

const styles = theme => ({
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  root: {
    ...theme.typography.button,
    backgroundColor: 'lightgray',
    padding: theme.spacing.unit,
  },
});

const NumberConverter = (props) => {
  const {
    classes,
    inputHandler,
    convertHandler,
    updateToggleStatus,
    formInformation,
    inputText,
  } = props;

  let title = null;
  let fieldTitle = null;

  if (formInformation === 3) {
    title = 'Decimal to Roman';
    fieldTitle = 'Decimal Number';
  } else if (formInformation === 2) {
    title = 'Binary to Roman';
    fieldTitle = 'Binary Number';
  } else {
    title = 'Hexadecimal to Roman';
    fieldTitle = 'Hexa Number';
  }

  return (
    <Card className={classes.card}>
      <CardContent>
        <ToggleView updateToggleStatus={updateToggleStatus} />
        <br />
        <TypographyView title={title} />
        <TextFieldView inputHandler={inputHandler} inputText={inputText} fieldTitle={fieldTitle}/>
        <br />
        <IconLabelButtons
          convertHandler={convertHandler}
        />
      </CardContent>
    </Card>
  );
};

NumberConverter.defaultProps = {
  classes: null,
};

NumberConverter.propTypes = {
  classes: PropTypes.shape({ object: PropTypes.object }),
  inputHandler: PropTypes.func.isRequired,
  convertHandler: PropTypes.func.isRequired,
  updateToggleStatus: PropTypes.func.isRequired,
  formInformation: PropTypes.number.isRequired,
  inputText: PropTypes.string.isRequired,
};

export default withStyles(styles)(NumberConverter);
