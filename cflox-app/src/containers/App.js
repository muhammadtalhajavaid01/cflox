import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import updateConversionHistory from '../actions/conversion-history-action';
import updateToggleStatus from '../actions/form-information-action';
import GridViewLayout from './GridViewLayout/GridViewLayout';
import { decimalToRoman } from '../services/conversion';
import SweetAlertView from '../components/SweetAlert/SweetAlert';
import { isValidDecimal, isValidBinary, isValidHexaDecimal } from '../services/validation';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      inputText: '',
      show: false,
      alertText: null,
    };

    this.inputHandler = this.inputHandler.bind(this);
    this.convertHandler = this.convertHandler.bind(this);
    this.updateToggleStatus = this.updateToggleStatus.bind(this);
    this.okHandler = this.okHandler.bind(this);
  }

  inputHandler(event) {
    this.setState({ inputText: event.target.value });
  }

  convertHandler() {
    const { inputText } = this.state;
    const { formInformation } = this.props;
    let romanValue = null;
    let format = null;

    if (formInformation === 3) {
      format = 'Decimal';
      if (isValidDecimal(inputText)) {
        romanValue = decimalToRoman(inputText);
      } else {
        romanValue = 'Invalid input.. Try again';
      }
    } else if (formInformation === 2) {
      format = 'Binary';
      if (isValidBinary(inputText)) {
        const decimalValue = parseInt(inputText, 2);
        romanValue = decimalToRoman(decimalValue);
      } else {
        romanValue = 'Invalid input.. Try again';
      }
    } else {
      format = 'Hexadecimal';
      if (isValidHexaDecimal(inputText)) {
        const decimalValue = parseInt(inputText, 16);
        romanValue = decimalToRoman(decimalValue);
      } else {
        romanValue = 'Invalid input.. Try again';
      }
    }

    this.setState({ show: true, alertText: romanValue });

    this.props.updateConversionHistory({
      format,
      input: inputText,
      convertedValue: romanValue,
      dateTime: new Date(),
    });
  }

  okHandler() {
    this.setState({ show: false });
  }

  updateToggleStatus(status) {
    this.setState({ inputText: '' });
    this.props.updateToggleStatus(status);
  }

  render() {
    const {
      show,
      alertText,
      inputText,
    } = this.state;

    const { conversionHistory, formInformation } = this.props;
    return (
      <div>
        <GridViewLayout
          conversionHistory={conversionHistory}
          inputHandler={this.inputHandler}
          convertHandler={this.convertHandler}
          updateToggleStatus={this.updateToggleStatus}
          formInformation={formInformation}
          inputText={inputText}
        />
        <SweetAlertView
          title="Result"
          show={show}
          text={alertText}
          okHandler={this.okHandler}
        />

      </div>
    );
  }
}

// State -> Props
const mapStateToProps = state => ({
  elements: state.elements,
  message: state.message,
  conversionHistory: state.conversionHistory,
  formInformation: state.formInformation,
});

// Functions -> Props
const mapDispatchToProps = dispatch => ({
  updateConversionHistory(payload) {
    dispatch(updateConversionHistory(payload));
  },
  updateToggleStatus(status) {
    dispatch(updateToggleStatus(status));
  },
});

App.defaultProps = {
  conversionHistory: [],
};

const propTypes = {
  conversionHistory: PropTypes.array.isRequired,
  formInformation: PropTypes.number.isRequired,
};

App.propTypes = propTypes;
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
