import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItemView from '../../components/ListItem/ListItemView';

const styles = theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
});

const HistoryListing = (props) => {
  const { classes, conversionHistory } = props;
  return (
    <List className={classes.root}>
      {conversionHistory.map(obj => (
        <ListItemView historyItem={obj} />
      ))}
    </List>
  );
};

HistoryListing.defaultProps = {
  classes: null,
  conversionHistory: [],
};

HistoryListing.propTypes = {
  classes: PropTypes.shape({ object: PropTypes.object }),
  conversionHistory: PropTypes.array.isRequired,
};

export default withStyles(styles)(HistoryListing);
