import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import AppBarContainer from '../AppBar/AppBar';
import NumberConverter from '../NumberConverter/NumberConverter';
import HistoryListing from '../HistoryListing/HistoryListing';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

const GridViewLayout = ({
  classes,
  conversionHistory,
  inputHandler,
  convertHandler,
  updateToggleStatus,
  formInformation,
  inputText,
}) => (
  <div className={classes.root}>
    <Grid container spacing={24}>
      <Grid item md={7} xs={12} s={12}>
        <Paper className={classes.paper}>
          <AppBarContainer title="Conversion Form" />
          <NumberConverter
            inputHandler={inputHandler}
            convertHandler={convertHandler}
            updateToggleStatus={updateToggleStatus}
            formInformation={formInformation}
            inputText={inputText}
          />
        </Paper>
      </Grid>
      <Grid item md={5} xs={12} s={12}>
        <Paper className={classes.paper}>
          <AppBarContainer title="Conversion History" />
          {
            conversionHistory.length > 0
              ? <HistoryListing conversionHistory={conversionHistory} />
              : <div><br/> <br/> <div> No Conversion History</div></div>
            }
        </Paper>
      </Grid>
    </Grid>
  </div>
);

GridViewLayout.defaultProps = {
  classes: null,
  conversionHistory: [],
};

GridViewLayout.propTypes = {
  classes: PropTypes.shape({ object: PropTypes.object }),
  conversionHistory: PropTypes.array.isRequired,
  inputHandler: PropTypes.func.isRequired,
  convertHandler: PropTypes.func.isRequired,
  updateToggleStatus: PropTypes.func.isRequired,
  formInformation: PropTypes.number.isRequired,
  inputText: PropTypes.string.isRequired,
};

export default withStyles(styles)(GridViewLayout);
