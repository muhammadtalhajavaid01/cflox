import * as types from '../constants/actions-types';

const defaultState = 3;

const formInformation = (state = defaultState, action) => {
  switch (action.type) {
    case types.UPDATE_TOGGLE_STATUS: {
      return action.status;
    }

    default:
      return state;
  }
};

export default formInformation;
