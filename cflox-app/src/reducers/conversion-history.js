import * as types from '../constants/actions-types';

const conversionHistoryLocalStorage = window.localStorage.getItem('conversionHistory');
let defaultState = [];
if (conversionHistoryLocalStorage && JSON.parse(conversionHistoryLocalStorage)) {
  defaultState = JSON.parse(conversionHistoryLocalStorage);
}

const conversionHistory = (state = defaultState, action) => {
  switch (action.type) {
    case types.UPDATE_CONVERSION_HISTORY: {
      const history = state.slice();

      if (history.length === 5) {
        history.pop();
      }

      history.unshift(action.history);
      window.localStorage.setItem('conversionHistory', JSON.stringify(history));
      return history;
    }

    default:
      return state;
  }
};

export default conversionHistory;
