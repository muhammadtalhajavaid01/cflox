import { combineReducers } from 'redux';

import conversionHistory from './conversion-history';
import formInformation from './form-information';

export default combineReducers({
  conversionHistory,
  formInformation,
});
