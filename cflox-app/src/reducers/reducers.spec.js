import conversionHistory from './conversion-history';
import updateConversionHistory from '../actions/conversion-history-action';
import * as types from '../constants/actions-types';

import formInformation from './form-information';
import updateToggleStatus from '../actions/form-information-action';

describe('conversionHistory', () => {
  it('Returns the initial state of conversionHistory element', () => {
    expect(conversionHistory([], {})).toEqual([]);
  });
});

describe('conversionHistory', () => {
  it('Adding new state in history', () => {
    const newState = {
      format: 'Decimal',
      input: 10.1,
      convertedValue: 'X',
      dateTime: new Date(),
    };

    expect(conversionHistory(newState, {}).length > 0);
  });
});

describe('updateConversionHistory', () => {
  it('should create an action to add a new state', () => {
    const newState = {
      format: 'Decimal',
      input: 10.1,
      convertedValue: 'X',
      dateTime: new Date(),
    };
    const expectedAction = {
      type: types.UPDATE_CONVERSION_HISTORY,
      history: newState,
    };
    expect(updateConversionHistory(newState)).toEqual(expectedAction);
  });
});

describe('formInformation', () => {
  it('Returns the initial state of conversionHistory element', () => {
    expect(formInformation(undefined, {})).toEqual(3);
  });
});

describe('updateToggleStatus', () => {
  it('Adding new state in toggle status', () => {
    const newState = 1;

    expect(formInformation(newState, {}).length > 0);
  });
});

describe('updateToggleStatus', () => {
  it('should create an action to update toggle state', () => {
    const newState = 2;
    const expectedAction = {
      type: types.UPDATE_TOGGLE_STATUS,
      status: newState,
    };
    expect(updateToggleStatus(newState)).toEqual(expectedAction);
  });
});
